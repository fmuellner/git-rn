/* grn-string-set-map.h
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "grn-strset.h"

G_BEGIN_DECLS

typedef struct _GrnStrsetDict     GrnStrsetDict;

GrnStrsetDict *grn_strset_dict_new  (void);
void           grn_strset_dict_free (GrnStrsetDict *dict);

void           grn_strset_dict_add  (GrnStrsetDict  *dict,
                                     const char     *key,
                                     const char     *value);

typedef GHashTableIter GrnStrsetDictIter;

void           grn_strset_dict_iter_init (GrnStrsetDictIter *iter,
                                          GrnStrsetDict     *dict);
gboolean       grn_strset_dict_iter_next (GrnStrsetDictIter  *iter,
                                          char              **key,
                                          GrnStrset         **values);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GrnStrsetDict, grn_strset_dict_free)

G_END_DECLS

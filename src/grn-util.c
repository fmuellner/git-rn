/* grn-util.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "grn-util.h"

GgitRepository *
grn_util_open_repository_cwd (GError **error)
{
  g_autoptr(GFile) cwd = NULL;
  g_autoptr(GFile) loc = NULL;

  cwd = g_file_new_for_path (g_get_current_dir ());
  loc = ggit_repository_discover (cwd, error);

  if (loc != NULL)
    return ggit_repository_open (loc, error);

  return NULL;
}

GList *
grn_util_get_commits_for_range (GgitRepository  *repo,
                                const char      *range,
                                GError         **error)
{
  g_autoptr (GgitRevisionWalker) walker = NULL;

  walker = g_initable_new (GGIT_TYPE_REVISION_WALKER, NULL, error,
                           "repository", repo,
                           NULL);

  if (walker == NULL)
    return NULL;

  GgitSortMode sort_mode = GGIT_SORT_TOPOLOGICAL | GGIT_SORT_REVERSE;
  ggit_revision_walker_set_sort_mode (walker, sort_mode);

  GError *tmp_error = NULL;
  ggit_revision_walker_push_range (walker, range, &tmp_error);

  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  GList *commits = NULL;
  GgitOId *oid;
  for (oid = ggit_revision_walker_next (walker, error);
       oid != NULL;
       oid = ggit_revision_walker_next (walker, error))
    {
      GgitObject *commit;
      commit = ggit_repository_lookup (repo, oid, GGIT_TYPE_COMMIT, error);
      ggit_oid_free (oid);

      if (commit == NULL)
        {
          g_list_free_full (commits, g_object_unref);
          return NULL;
        }

      commits = g_list_prepend (commits, commit);
    }

  return g_list_reverse (commits);
}

static int
print_cb (GgitDiffDelta *delta,
          GgitDiffHunk  *hunk G_GNUC_UNUSED,
          GgitDiffLine  *line G_GNUC_UNUSED,
          gpointer       user_data)
{
  GPtrArray *paths = user_data;
  GgitDiffFile *new_file = ggit_diff_delta_get_new_file (delta);
  const char *path = NULL;

  if (new_file)
    path = ggit_diff_file_get_path (new_file);
  if (path)
    g_ptr_array_add (paths, g_strdup (path));

  return 0;
}

char **
grn_util_get_changed_paths_for_commit (GgitCommit  *commit,
                                       GError     **error)
{
  g_autoptr(GgitRepository) repo = NULL;
  g_autoptr(GgitCommit) parent = NULL;
  g_autoptr(GgitTree) old_tree = NULL;
  g_autoptr(GgitTree) new_tree = NULL;
  g_autoptr(GgitDiff) diff = NULL;
  GPtrArray *paths;

  repo = ggit_object_get_owner (GGIT_OBJECT (commit));
  parent = ggit_commit_get_nth_ancestor (commit, 1, error);

  if (parent == NULL)
    return NULL;

  old_tree = ggit_commit_get_tree (parent);
  new_tree = ggit_commit_get_tree (commit);
  diff = ggit_diff_new_tree_to_tree (repo, old_tree, new_tree, NULL, error);

  if (diff == NULL)
    return NULL;

  paths = g_ptr_array_new ();
  ggit_diff_print (diff, GGIT_DIFF_FORMAT_NAME_ONLY,
                   print_cb, (gpointer*)paths, NULL);
  g_ptr_array_add (paths, NULL);

  return (char **)g_ptr_array_free (paths, FALSE);
}

void
grn_util_print_line_wrapped (const char *text,
                             const char *separator,
                             guint       indent)
{
  glong len = g_utf8_strlen (text, -1);

  if (len == 0)
    return;

  guint max_line = 80 - indent;
  gsize sep_trail = strlen (separator) - 1;

  guint start = 0, end;
  while (start < len)
    {
      g_autofree char *line = NULL;
      const char *s = NULL;

      if (len - start > max_line)
        {
          s = g_utf8_offset_to_pointer (text, start + max_line);
          s = g_strrstr_len (text, s - text + sep_trail, separator) + 1;
        }

      end = s ? (guint)(g_utf8_pointer_to_offset (text, s)) : len;

      line = g_utf8_substring (text, start, end);
      g_print ("%*s%s\n", indent, "", line);
      start = end + sep_trail;
    }
}

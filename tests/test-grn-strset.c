/* test-grn-strset.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>

#include "grn-strset.h"

static void
grn_strset_test_empty (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  str = grn_strset_join (set, "test");
  g_assert_cmpstr (str, ==, "");
}

static void
grn_strset_test_add (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  grn_strset_add (set, "test");

  str = grn_strset_join (set, "-");
  g_assert_cmpstr (str, ==, "test");
}

static void
grn_strset_test_unique (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  grn_strset_add (set, "test");
  grn_strset_add (set, "test");

  str = grn_strset_join (set, "-");
  g_assert_cmpstr (str, ==, "test");
}

static void
grn_strset_test_add2 (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  grn_strset_add (set, "test");
  grn_strset_add (set, "test");
  grn_strset_add (set, "test2");

  str = grn_strset_join (set, "-");
  g_assert_cmpstr (str, ==, "test-test2");
}

static void
grn_strset_test_unique2 (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  grn_strset_add (set, "test");
  grn_strset_add (set, "test");
  grn_strset_add (set, "test2");
  grn_strset_add (set, "test");

  str = grn_strset_join (set, "-");
  g_assert_cmpstr (str, ==, "test-test2");
}

static void
grn_strset_test_order (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  grn_strset_add (set, "test");
  grn_strset_add (set, "test");
  grn_strset_add (set, "test2");
  grn_strset_add (set, "test");
  grn_strset_add (set, "00abc");

  str = grn_strset_join (set, "-");
  g_assert_cmpstr (str, ==, "test-test2-00abc");
}

static int
my_sort (const char **a,
         const char **b)
{
  return strcmp (*a, *b);
}

static void
grn_strset_test_sort (void)
{
  g_autoptr(GrnStrset) set = grn_strset_new ();
  g_autofree char *str = NULL;

  grn_strset_add (set, "test");
  grn_strset_add (set, "test");
  grn_strset_add (set, "test2");
  grn_strset_add (set, "test");
  grn_strset_add (set, "00abc");

  grn_strset_sort (set, my_sort);

  str = grn_strset_join (set, "-");
  g_assert_cmpstr (str, ==, "00abc-test-test2");
}

int
main (int   argc,
      char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func("/strset/empty", grn_strset_test_empty);
  g_test_add_func("/strset/add", grn_strset_test_add);
  g_test_add_func("/strset/unique", grn_strset_test_unique);
  g_test_add_func("/strset/add2", grn_strset_test_add2);
  g_test_add_func("/strset/unique2", grn_strset_test_unique2);
  g_test_add_func("/strset/order", grn_strset_test_order);
  g_test_add_func("/strset/sort", grn_strset_test_sort);

  return g_test_run ();
}

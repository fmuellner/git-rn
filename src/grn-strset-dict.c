/* grn-strset-dict.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "grn-strset-dict.h"
#include "grn-strset.h"

struct _GrnStrsetDict {
  GHashTable   *dict;
  GList        *sort_order;
};

typedef struct {
  GrnStrsetDict *dict;
  GList         *position;
  gpointer       dummy3;
  int            dummy4;
  gboolean       dummy5;
  gpointer       dummy6;
} RealIter;

GrnStrsetDict *
grn_strset_dict_new (void)
{
  GrnStrsetDict *dict = g_new0 (GrnStrsetDict, 1);
  dict->dict = g_hash_table_new_full (g_str_hash, g_str_equal,
                                      g_free, (GDestroyNotify)grn_strset_free);
  return dict;
}

void
grn_strset_dict_free (GrnStrsetDict *dict)
{
  g_hash_table_destroy (dict->dict);
  g_list_free (dict->sort_order);
}

void
grn_strset_dict_add (GrnStrsetDict *dict,
                     const char    *key,
                     const char    *value)
{
  GrnStrset *set = g_hash_table_lookup (dict->dict, key);

  if (set == NULL)
    {
      char *cpy = g_strdup (key);

      set = grn_strset_new ();
      g_hash_table_insert (dict->dict, cpy, set);

      dict->sort_order = g_list_prepend (dict->sort_order, cpy);
    }
  grn_strset_add (set, value);
}

void
grn_strset_dict_iter_init (GrnStrsetDictIter *iter,
                           GrnStrsetDict     *dict)
{
  RealIter *ri = (RealIter *)iter;
  ri->dict = dict;
  ri->position = g_list_last (dict->sort_order);
}

gboolean
grn_strset_dict_iter_next (GrnStrsetDictIter  *iter,
                           char              **key,
                           GrnStrset         **values)
{
  RealIter *ri = (RealIter *)iter;
  char *next_key;

  if (ri->position == NULL)
    return FALSE;

  next_key = ri->position->data;

  if (key != NULL)
    *key = next_key;

  if (values != NULL)
      *values = g_hash_table_lookup (ri->dict->dict, next_key);

  ri->position = g_list_previous (ri->position);

  return TRUE;
}

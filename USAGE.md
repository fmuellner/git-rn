git-rn(1) -- Generate release news templates from git history
=============================================================

## SYNOPSIS

**git rn** [`<`_options_`>`] `<`_revision range_`>`

## DESCRIPTION

**git rn** scans commit messages of a specified range for issue tracker
references to generate output like the following:

    0.42.1
    ======
    * https://gitlab.gnome.org/GNOME/foobar/issues/11 [Rupert; #11]
    * https://gitlab.gnome.org/GNOME/foobar/issues/23 [Angela; #23]
    * https://gitlab.gnome.org/GNOME/foobar/merge_requests/4 [Judy; !4]
    * https://gitlab.gnome.org/GNOME/foobar/issues/17 [Tux, Wilber; #17]

    Contributors:
      Angela Avery, Judy Garland, Rupert Monkey, Tux Penguin, Wilber

    Translators:
      Emily Brontë [en_GB], Anne Brontë [fr], Charlotte Brontë [it]

## OPTIONS

 * **-V**, **--release-version**=_VERSION_  
    Specify a release version that appears as header of the generated output.

 * **-n**, **--release-name**=_NAME_  
    Specify a release name that is displayed alongside the version. Note that
    this option only has an effect if a release version is specified as well.

 * **-o**, **--output**=_OUTPUT_  
    Specify which output should be generated, where OUTPUT can be one of
    _bugs_, _contributors_, _translators_ and _all_. If omitted, **git rn**
    will generate full output as if _all_ had been specified.

    This is useful for example to update the translators section when new
    commits happened while editing a previously generated release news template.

## EXAMPLES

* **git rn -V 0.42.1 0.42.0..HEAD**  
    Generate release notes output for a 0.42.1 release based on all commits
    between the 0.42.0 tag and the current branch head

* **git rn -o translators 0.42.0..HEAD**  
    As above, but only ouput the translators section

* **git rn $(git describe --abbrev=0)..HEAD**  
    Generate release notes for all commits since the most recent tag

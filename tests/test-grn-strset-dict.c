/* test-grn-strset-dict.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>

#include "grn-strset-dict.h"

static char *
grn_strset_dict_to_string (GrnStrsetDict *dict)
{
  GString *str;
  GrnStrsetDictIter iter;
  GrnStrset *values;
  char *key;

  str = g_string_new (NULL);

  grn_strset_dict_iter_init (&iter, dict);
  while (grn_strset_dict_iter_next (&iter, &key, &values))
    {
      g_autofree char *values_str = grn_strset_join (values, ",");
      g_string_append_printf (str, "%s[%s]", key, values_str);
    }

  return g_string_free (str, FALSE);
}

static void
grn_strset_dict_test_empty (void)
{
  g_autoptr(GrnStrsetDict) dict = grn_strset_dict_new ();
  g_autofree char *str = NULL;

  str = grn_strset_dict_to_string (dict);
  g_assert_cmpstr (str, ==, "");
}

static void
grn_strset_dict_test_add (void)
{
  g_autoptr(GrnStrsetDict) dict = grn_strset_dict_new ();
  g_autofree char *str = NULL;

  grn_strset_dict_add (dict, "test", "0");

  str = grn_strset_dict_to_string (dict);
  g_assert_cmpstr (str, ==, "test[0]");
}

static void
grn_strset_dict_test_add2 (void)
{
  g_autoptr(GrnStrsetDict) dict = grn_strset_dict_new ();
  g_autofree char *str = NULL;

  grn_strset_dict_add (dict, "test", "0");
  grn_strset_dict_add (dict, "test2", "42");

  str = grn_strset_dict_to_string (dict);
  g_assert_cmpstr (str, ==, "test[0]test2[42]");
}

static void
grn_strset_dict_test_grouping (void)
{
  g_autoptr(GrnStrsetDict) dict = grn_strset_dict_new ();
  g_autofree char *str = NULL;

  grn_strset_dict_add (dict, "test", "0");
  grn_strset_dict_add (dict, "test2", "42");
  grn_strset_dict_add (dict, "test", "1");

  str = grn_strset_dict_to_string (dict);
  g_assert_cmpstr (str, ==, "test[0,1]test2[42]");
}

static void
grn_strset_dict_test_order (void)
{
  g_autoptr(GrnStrsetDict) dict = grn_strset_dict_new ();
  g_autofree char *str = NULL;

  grn_strset_dict_add (dict, "zzzz", "99");
  grn_strset_dict_add (dict, "argh", "00");
  grn_strset_dict_add (dict, "mooo", "42");

  str = grn_strset_dict_to_string (dict);
  g_assert_cmpstr (str, ==, "zzzz[99]argh[00]mooo[42]");
}

int
main (int   argc,
      char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/strset-dict/empty", grn_strset_dict_test_empty);
  g_test_add_func ("/strset-dict/add", grn_strset_dict_test_add);
  g_test_add_func ("/strset-dict/add2", grn_strset_dict_test_add2);
  g_test_add_func ("/strset-dict/grouping", grn_strset_dict_test_grouping);
  g_test_add_func ("/strset-dict/order", grn_strset_dict_test_order);

  return g_test_run ();
}

# git-rn

git-rn is a tool to generate release notes templates from git history.
See the [usage instructions][usage] for details.

## How to report bugs

If you found a problem, please report the issue to the
GNOME [bug tracking system][bug-tracker].

[bug-tracker]: https://gitlab.gnome.org/fmuellner/git-rn/issues
[usage]: USAGE.md

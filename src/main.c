/* main.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>

#include <glib.h>
#include <glib/gi18n.h>

#include <libgit2-glib/ggit.h>

#include "config.h"

#include "grn-strset-dict.h"
#include "grn-util.h"

typedef enum {
  GRN_OUTPUT_ALL,
  GRN_OUTPUT_BUGS,
  GRN_OUTPUT_CONTRIBUTORS,
  GRN_OUTPUT_TRANSLATORS
} GrnOutputType;

static GrnOutputType opt_output = GRN_OUTPUT_ALL;

#define should_show(O) (opt_output == (O) || opt_output == GRN_OUTPUT_ALL)
#define show_bugs should_show (GRN_OUTPUT_BUGS)
#define show_contributors should_show (GRN_OUTPUT_CONTRIBUTORS)
#define show_translators should_show (GRN_OUTPUT_TRANSLATORS)

static gboolean
arg_output_cb (const char  *option,
               const char  *value,
               gpointer     user_data G_GNUC_UNUSED,
               GError     **error)
{
  const char *choices[] = { "all", "bugs", "contributors", "translators" };
  gsize n_choices = G_N_ELEMENTS (choices);
  guint i;

  for (i = 0; i < n_choices; i++)
    if (g_str_has_prefix (choices[i], value))
      {
        opt_output = i;
        return TRUE;
      }

  g_set_error (error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
               _("Invalid value %s for option %s"), value, option);
  return FALSE;
}

#define ISSUE_REGEX "\\bhttps?://(bugzilla|gitlab)\\.gnome\\.org/\\D*\\d+\\b"
static char *
commit_get_issue_url (GgitCommit *commit)
{
  static GRegex *regex = NULL;

  const char *message = ggit_commit_get_message (commit);
  const char *line;
  size_t len = strlen (message);

  for (line = g_strrstr_len (message, len - 1, "\n");
       line;
       line = g_strrstr_len (message, line - message, "\n"))
    {
      g_autoptr(GMatchInfo) match_info = NULL;

      if (regex == NULL)
        regex = g_regex_new (ISSUE_REGEX, 0, 0, NULL);

      if (g_regex_match (regex, line, 0, &match_info))
        return g_match_info_fetch (match_info, 0);
    }
  return NULL;
}

static char *
commit_get_author_full_name (GgitCommit *commit)
{
  g_autoptr(GgitSignature) author = ggit_commit_get_author (commit);
  return g_strdup (ggit_signature_get_name (author));
}

static char *
commit_get_author_name (GgitCommit *commit)
{
  char *full_name = commit_get_author_full_name (commit);
  char *end = strstr (full_name, " ");

  if (end)
    *end = '\0';
  return full_name;
}

static gboolean
changed_path_is_translation (const char  *path,
                             char       **lang)
{
  if (!g_str_has_prefix (path, "po/") || !g_str_has_suffix (path, ".po"))
    return FALSE;

  if (lang != NULL)
    *lang = g_strndup (path + strlen ("po/"),
                       strlen (path) - strlen ("po/.po"));
  return TRUE;
}

static void
print_header (const char *version,
              const char *name)
{
  if (version == NULL)
    return;

  g_autofree char *quoted_name = NULL;
  if (name)
    quoted_name = g_strdup_printf ("“%s”", name);

  g_autofree char *title = g_strjoin (" — ", version, quoted_name, NULL);
  g_print ("%s\n", title);
  for (long len = g_utf8_strlen (title, -1); len; len--)
    g_print ("%c", '=');
  g_print ("\n");
}

static void
print_issues (GrnStrsetDict *issues)
{
  GrnStrsetDictIter iter;
  GrnStrset *authors;
  char *url;

  grn_strset_dict_iter_init (&iter, issues);
  while (grn_strset_dict_iter_next (&iter, &url, &authors))
    {
      g_autofree char *authors_str = grn_strset_join (authors, ", ");
      const char *id;
      gboolean is_mr;
      gsize len;

      for (len = strlen (url); g_ascii_isdigit (url[len - 1]); len--)
        ;
      id = url + len;

      is_mr = (strstr (url, "merge_request") != NULL);

      g_print ("* %s [%s; %c%s]\n", url, authors_str, is_mr ? '!' : '#', id);
    }
}

static int
surname_cmp (const char **a,
             const char **b)
{
  const char *sur_a, *sur_b, *s;

  s = g_strrstr (*a, " ");
  sur_a = s ? s + 1 : *a;

  s = g_strrstr (*b, " ");
  sur_b = s ? s + 1 : *b;

  return g_utf8_collate (sur_a, sur_b);
}

#define INDENT 2
static void
print_contributors (GrnStrset *contributors)
{
  g_autofree char *str = NULL;

  grn_strset_sort (contributors, surname_cmp);

  str = grn_strset_join (contributors, ", ");
  if (*str == '\0')
    return;

  g_print ("Contributors:\n");
  grn_util_print_line_wrapped (str, ", ", INDENT);
}

static void
print_translators (GrnStrsetDict *translators)
{
  g_autoptr(GPtrArray) arr = g_ptr_array_new_with_free_func (g_free);
  g_autofree char *str = NULL;
  GrnStrsetDictIter iter;
  GrnStrset *langs;
  char *name;

  grn_strset_dict_iter_init (&iter, translators);
  while (grn_strset_dict_iter_next (&iter, &name, &langs))
    {
      g_autofree char *s = grn_strset_join (langs, ", ");
      g_ptr_array_add (arr, g_strdup_printf ("%s [%s]", name, s));
    }
  g_ptr_array_add (arr, NULL);

  str = g_strjoinv (", ", (char **)arr->pdata);
  if (*str == '\0')
    return;

  g_print ("Translators:\n");
  grn_util_print_line_wrapped (str, ", ", INDENT);
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GError) err = NULL;

  gboolean opt_show_version = FALSE;
  char  *opt_version = NULL;
  char  *opt_name = NULL;
  char **opt_range = NULL;
  const GOptionEntry option_entries[] =
    {
      { "release-version", 'V',
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_STRING, &opt_version,
        N_("The version of the release news entry"),
        N_("<version>") },
      { "release-name", 'n',
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_STRING, &opt_name,
        N_("The name of the release news entry"),
        N_("<name>") },
      { "output", 'o',
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_CALLBACK, arg_output_cb,
        N_("The release news output to generate"),
        "bugs|contributors|translators|all" },
      { "version", 'v',
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_NONE, &opt_show_version,
        N_("Show version and exit"),
        NULL },
      { G_OPTION_REMAINING, 0,
        G_OPTION_FLAG_NONE,
        G_OPTION_ARG_STRING_ARRAY, &opt_range,
        NULL,
        N_("<REVISION RANGE>") },
      { NULL }
    };

  g_autoptr(GgitRepository) repo = NULL;
  g_autolist(GObject) commits = NULL;

  g_autoptr(GrnStrsetDict) issues = NULL;
  g_autoptr(GrnStrset) contributors = NULL;
  g_autoptr(GrnStrsetDict) translators = NULL;

  g_set_prgname ("git-rn");

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  setlocale (LC_ALL, "");

  context = g_option_context_new (_(" — generate a release news template from git history"));
  g_option_context_add_main_entries (context, option_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (context, &argc, &argv, &err))
    goto error;

  if (opt_show_version)
    {
      g_print (PACKAGE_VERSION "\n");
      return 0;
    }

  if (opt_range == NULL)
    {
      g_printerr ("Need a revision range to operate on\n");
      return 1;
    }

  ggit_init ();

  repo = grn_util_open_repository_cwd (&err);

  if (err != NULL)
    goto error;

  commits = grn_util_get_commits_for_range (repo, opt_range[0], &err);

  if (err != NULL)
    goto error;

  issues = grn_strset_dict_new ();
  contributors = grn_strset_new ();
  translators = grn_strset_dict_new ();

  for (GList *l = commits; l; l = l->next)
    {
      GgitCommit *commit = l->data;
      g_autofree char *url = NULL;
      g_auto(GStrv) paths = NULL;
      char **p;

      if (show_bugs)
        url = commit_get_issue_url (commit);

      if (url)
        {
          g_autofree char *name = commit_get_author_name (commit);
          grn_strset_dict_add (issues, url, name);
        }

      if (!(show_contributors || show_translators))
        continue;

      paths = grn_util_get_changed_paths_for_commit (commit, &err);

      if (err != NULL)
        goto error;

      for (p = paths; p && *p; p++)
        {
          g_autofree char *name = commit_get_author_full_name (commit);
          g_autofree char *lang = NULL;

          if (changed_path_is_translation (*p, &lang))
            grn_strset_dict_add (translators, name, lang);
          else
            grn_strset_add (contributors, name);
        }
    }

  print_header (opt_version, opt_name);
  if (show_bugs)
    print_issues (issues);
  if (show_contributors)
    {
      if (opt_version != NULL || show_bugs)
        g_print ("\n");
      print_contributors (contributors);
    }
  if (show_translators)
    {
      if (opt_version != NULL || show_contributors)
        g_print ("\n");
      print_translators (translators);
    }

  return 0;

error:
  g_printerr ("%s\n", err->message);
  return 1;
}

/* grn-string-set.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "grn-strset.h"

struct _GrnStrset {
  GPtrArray *values;
  GHashTable *registry;
};

GrnStrset *
grn_strset_new (void)
{
  GrnStrset *set = g_new0 (GrnStrset, 1);

  set->registry = g_hash_table_new (g_str_hash, g_str_equal);
  set->values = g_ptr_array_new_with_free_func (g_free);

  /* Make sure the array is always NULL-terminated */
  g_ptr_array_add (set->values, NULL);

  return set;
}

void
grn_strset_free (GrnStrset *set)
{
  g_hash_table_destroy (set->registry);
  g_ptr_array_unref (set->values);
  g_free (set);
}

void
grn_strset_add (GrnStrset  *set,
                const char *value)
{
  char *cpy;

  if (g_hash_table_contains (set->registry, value))
    return;

  cpy = g_strdup (value);
  g_hash_table_add (set->registry, cpy);
  g_ptr_array_insert (set->values, set->values->len - 1, cpy);
}

static int
sort_wrapper (gconstpointer ap,
              gconstpointer bp,
              gpointer      data)
{
  const char **a = (const char **)ap;
  const char **b = (const char **)bp;
  GrnStrsetSortFunc func = data;

  /* Keep the NULL-terminator at the end */
  if (*a == NULL || *b == NULL)
    return !!(*a == NULL) - !!(*b == NULL);
  return func(a, b);
}

void
grn_strset_sort (GrnStrset         *set,
                 GrnStrsetSortFunc  func)
{
  g_ptr_array_sort_with_data (set->values, sort_wrapper, func);
}

char *
grn_strset_join (GrnStrset  *set,
                 const char *separator)
{
  return g_strjoinv (separator, (char **)set->values->pdata);
}

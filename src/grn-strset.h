/* grn-string-set.h
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

typedef struct _GrnStrset GrnStrset;

typedef int (*GrnStrsetSortFunc) (const char **, const char **);

GrnStrset *grn_strset_new  (void);
void       grn_strset_free (GrnStrset *set);

void       grn_strset_add  (GrnStrset  *set,
                            const char *value);

void       grn_strset_sort (GrnStrset         *set,
                            GrnStrsetSortFunc  func);

char      *grn_strset_join (GrnStrset  *set,
                            const char *separator);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GrnStrset, grn_strset_free)

G_END_DECLS

/* test-grn-util.c
 *
 * Copyright 2018 Florian Müllner <fmuellner@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <locale.h>

#include "grn-util.h"

typedef struct {
  GgitRepository *repo;
} GrnUtilGitFixture;

typedef struct {} DummyFixture;

static GString *output = NULL;

static void
grn_util_print_handler (const char *str)
{
  g_string_append (output, str);
}

static void
grn_util_git_fixture_set_up (GrnUtilGitFixture *fixture,
                             gconstpointer      data G_GNUC_UNUSED)
{
  g_autoptr(GError) err = NULL;

  chdir (g_test_get_filename (G_TEST_DIST, ".", NULL));

  ggit_init ();

  fixture->repo = grn_util_open_repository_cwd (&err);

  if (g_error_matches (err, GGIT_ERROR, GGIT_ERROR_NOTFOUND))
    g_test_skip ("srcdir is not a git checkout");
  else if (fixture->repo == NULL)
    g_test_fail ();
}

static void
grn_util_print_fixture_set_up (DummyFixture  *fixture G_GNUC_UNUSED,
                               gconstpointer  data    G_GNUC_UNUSED)
{
  GPrintFunc old_print_handler;

  old_print_handler = g_set_print_handler (grn_util_print_handler);
  g_assert (old_print_handler == NULL);

  output = g_string_new (NULL);
}

static void
grn_util_print_fixture_tear_down (DummyFixture  *fixture G_GNUC_UNUSED,
                                  gconstpointer  data    G_GNUC_UNUSED)
{
  g_set_print_handler (NULL);

  g_string_free (output, TRUE);
}

static void
grn_util_test_get_commits_pass (GrnUtilGitFixture *fixture,
                                gconstpointer      data G_GNUC_UNUSED)
{
  g_autolist(GObject) commits = NULL;
  g_autoptr(GError) err = NULL;

  if (g_test_failed ())
    return;

  commits = grn_util_get_commits_for_range (fixture->repo,
                                            "master~2..master", &err);
  g_assert_no_error (err);
  g_assert_nonnull (commits);
  g_assert_cmpint (g_list_length (commits), ==, 2);
}

static void
grn_util_test_get_commits_fail (GrnUtilGitFixture *fixture,
                                gconstpointer      data G_GNUC_UNUSED)
{
  g_autolist(GObject) commits = NULL;
  g_autoptr(GError) err = NULL;

  if (g_test_failed ())
    return;

  commits = grn_util_get_commits_for_range (fixture->repo,
                                            "invalid-ref..", &err);
  g_assert_error (err, GGIT_ERROR, GGIT_ERROR_NOTFOUND);
  g_assert_null (commits);
}

static void
grn_util_test_get_commits_order (GrnUtilGitFixture *fixture,
                                 gconstpointer      data G_GNUC_UNUSED)
{
  g_autolist(GObject) commits = NULL;
  g_autoptr(GgitCommit) parent = NULL;
  g_autoptr(GgitOId) parent_id = NULL;
  g_autoptr(GgitOId) older_id = NULL;

  if (g_test_failed ())
    return;

  commits = grn_util_get_commits_for_range (fixture->repo,
                                            "master~2..master", NULL);

  GgitCommit *older = g_list_nth_data (commits, 0);
  GgitCommit *newer = g_list_nth_data (commits, 1);

  parent = ggit_commit_get_nth_ancestor (newer, 1, NULL);
  parent_id = ggit_object_get_id (GGIT_OBJECT (parent));
  older_id = ggit_object_get_id (GGIT_OBJECT (older));

  g_assert_true (ggit_oid_equal (parent_id, older_id));
}

static void
grn_util_test_get_changed_paths (GrnUtilGitFixture *fixture,
                                 gconstpointer      data G_GNUC_UNUSED)
{
  g_autolist(GObject) commits = NULL;
  g_autoptr(GError) err = NULL;
  g_auto(GStrv) paths = NULL;

  if (g_test_failed ())
    return;

  commits = grn_util_get_commits_for_range (fixture->repo, "0.1.0^..0.1.0", &err);

  if (g_error_matches (err, GGIT_ERROR, GGIT_ERROR_NOTFOUND))
    {
      g_test_skip ("need 0.1.0 release tag to work");
      return;
    }

  g_assert_nonnull (commits);

  paths = grn_util_get_changed_paths_for_commit (commits->data, &err);

  g_assert_no_error (err);

  const char *expected[] = { "NEWS", "meson.build", NULL };
  g_assert_cmpuint (g_strv_length ((char **)expected), ==, g_strv_length (paths));

  for (char **ptr = paths; ptr && *ptr; ptr++)
    g_assert_true (g_strv_contains (expected, *ptr));
}

static void
grn_util_test_print_line_wrapped_basic (DummyFixture  *fixture G_GNUC_UNUSED,
                                        gconstpointer  data    G_GNUC_UNUSED)
{
  const char *text =
    "Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, "
    "Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar";
  const char *wrapped_text =
    "                    Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar,\n"
    "                    Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar\n";

  grn_util_print_line_wrapped (text, ", ", 20);
  g_assert_cmpstr (output->str, ==, wrapped_text);
}

static void
grn_util_test_print_line_wrapped_edge (DummyFixture  *fixture G_GNUC_UNUSED,
		                       gconstpointer  data    G_GNUC_UNUSED)
{
  const char *text =
    "Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Fubar, "
    "Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar";
  const char *wrapped_text =
    "                    Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar, Fubar,\n"
    "                    Foo Bar, Foo Bar, Foo Bar, Foo Bar, Foo Bar\n";

  grn_util_print_line_wrapped (text, ", ", 20);
  g_assert_cmpstr (output->str, ==, wrapped_text);
}

static void
grn_util_test_print_line_wrapped_utf8 (DummyFixture *fixture G_GNUC_UNUSED,
                                       gconstpointer data    G_GNUC_UNUSED)
{
  const char *text =
    "Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, "
    "Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар";
  const char *wrapped_text =
    "                    Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар,\n"
    "                    Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар, Фуь Бар\n";

  grn_util_print_line_wrapped (text, ", ", 20);
  g_assert_cmpstr (output->str, ==, wrapped_text);
}

int
main (int   argc,
      char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/util/get-commits/pass", GrnUtilGitFixture, NULL,
              grn_util_git_fixture_set_up, grn_util_test_get_commits_pass,
              NULL);
  g_test_add ("/util/get-commits/fail", GrnUtilGitFixture, NULL,
              grn_util_git_fixture_set_up, grn_util_test_get_commits_fail,
              NULL);
  g_test_add ("/util/get-commits/order", GrnUtilGitFixture, NULL,
              grn_util_git_fixture_set_up, grn_util_test_get_commits_order,
              NULL);

  g_test_add ("/util/get-changed-paths/basic", GrnUtilGitFixture, NULL,
              grn_util_git_fixture_set_up, grn_util_test_get_changed_paths,
              NULL);

  g_test_add ("/util/print-line-wrapped/basic", DummyFixture, NULL,
              grn_util_print_fixture_set_up, grn_util_test_print_line_wrapped_basic,
              grn_util_print_fixture_tear_down);
  g_test_add ("/util/print-line-wrapped/edge", DummyFixture, NULL,
              grn_util_print_fixture_set_up, grn_util_test_print_line_wrapped_edge,
              grn_util_print_fixture_tear_down);
  g_test_add ("/util/print-line-wrapped/utf8", DummyFixture, NULL,
              grn_util_print_fixture_set_up, grn_util_test_print_line_wrapped_utf8,
              grn_util_print_fixture_tear_down);

  return g_test_run ();
}
